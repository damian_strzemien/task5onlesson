public class TaskResource {
    int checkNum;

    public synchronized void put(int i) {
        this.checkNum = i;
        notifyAll();
    }


    public synchronized int take() throws InterruptedException {
        while (checkNum == 0) {
            this.wait();
        }
        int effect = checkNum;
        checkNum = 0;
        return effect;
    }
}
