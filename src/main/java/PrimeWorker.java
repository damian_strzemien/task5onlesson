public class PrimeWorker implements Runnable {

    int primeNum;
    ResultResource saving;

    public PrimeWorker(int primeNum, ResultResource saving) {
        this.primeNum = primeNum;
        this.saving = saving;
    }

    @Override
    public void run(){
        if(PrimeMain.isPrime(primeNum)) {
            saving.save(primeNum + " true");
            System.out.println(primeNum + " is a prime ");
        } else {
            saving.save(primeNum + " false");
            System.out.println(primeNum + " is not prime ");

        }
    }
}
