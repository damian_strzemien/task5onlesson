import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class ResultResource {

    private File file;

    public ResultResource(String file) {
        this.file = new File(file);
    }

    public synchronized void save(String line) {
        try(BufferedWriter bw = new BufferedWriter(new FileWriter(file, true))) {
            bw.write(line + "\n");
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }


}
